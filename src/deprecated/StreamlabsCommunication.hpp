// this file is deprecated. It isn't used as a source anymore.

#pragma once
#include <QString>

using namespace std;

class StreamlabsCommunication{
public:
    StreamlabsCommunication(const QString &Voice, QString Text);
    QString parseLink();
    bool isSuccessful();
private:
    QString commandReadString(const QString &cmd);
    QString serverResponse;
    bool SuccessCheck;
};
