#include "voicegen.h"
#include "ui_voicegen.h"
#include <QMessageBox>
#include <QFileDialog>
#include <iostream>
#include <QDialog>
#include "TextToSpeech.h"
#include <array>
#include <filesystem>
#include <string>

VoiceGen::VoiceGen(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::VoiceGen),
    player(new QMediaPlayer(this)),
    tts(new TextToSpeech(player))
{
    m_ui->setupUi(this);
    m_ui->engineCombobox->setCurrentIndex(2);// I set it on Streamlabs because it doesn't need any dependancy and it just works out of the box
    this->on_engineCombobox_currentIndexChanged();
    m_ui->menuBar->hide();
    menubarShortcut = new QShortcut(QKeySequence("Ctrl+M"),this);
    connect(menubarShortcut, &QShortcut::activated, this , &VoiceGen::on_ctrlm_triggered);

    const auto snap = std::string(std::getenv("SNAP_NAME")?std::getenv("SNAP_NAME"):"") == "voicegen";
    //it seems that the GNOME desktop environment does not care about icons that we set in designer so we set costom icons if the user uses GNOME DE.
    try{
        const std::string desktop_environment = std::getenv("XDG_CURRENT_DESKTOP");
        auto actions = m_ui->mainToolBar->actions(); //list of action pointers
        if(desktop_environment == "GNOME" || snap)
        {
            if(snap)std::cout << "Voicegen detected a snap environment, Running workarounds...\n";
            for (int a = 0;const auto& icon_name:{"read", "stop" , "", "paste", "clear", "save-as", "save","","voicegenicon"})
            {
                actions[a]->setIcon(QIcon(":/mainresource/"+QString(icon_name)+".png"));
                actions[a]->setToolTip(actions[a]->text());
                actions[a]->setText("");
                a++;
            }
        }
        //text-speak is not XDG-standard. so we fallback to a safe icon (available in XDG) if it is not there.
        else if(!QIcon::hasThemeIcon("text-speak"))
        {
            if (QIcon::hasThemeIcon("audio-volume-high-symbolic"))actions[0]->setIcon(QIcon::fromTheme("audio-volume-high-symbolic"));
            else actions[0]->setIcon(QIcon::fromTheme("audio-volume-high"));
        }
    }
    catch(...){std::cout << "\e[0;33mWARNING: $XDG_CURRENT_DESKTOP doesn't exist. Maybe your distro/pkging doesn't support XDG.\e[0m\n";}
    if (!std::filesystem::exists("/usr/local/bin/nanotts") && !std::filesystem::exists("/usr/bin/nanotts") && !std::filesystem::exists("/app/bin/nanotts") && !snap)
    {
        //this is a hack to "hide an option"
        m_ui->engineCombobox->setItemData(0, false, Qt::UserRole -1);
        m_ui->engineCombobox->setItemData(0, QSize(0,0), Qt::SizeHintRole);
        m_ui->engineCombobox->setItemText(0,"");
        m_ui->engineCombobox->setItemData(1, false, Qt::UserRole -1);
        m_ui->engineCombobox->setItemData(1, QSize(0,0), Qt::SizeHintRole);
        m_ui->engineCombobox->setItemText(1,"");

    }
    else if (!std::filesystem::exists("/bin/ffmpeg") && !snap)
    {
        m_ui->engineCombobox->setItemData(1, false, Qt::UserRole -1);
        m_ui->engineCombobox->setItemData(1, QSize(0,0), Qt::SizeHintRole);
        m_ui->engineCombobox->setItemText(1,"");
    }
    connect(tts,SIGNAL(taskFinished(QString)),this,SLOT(on_ttsthread_finished(QString)));
}


VoiceGen::~VoiceGen() = default;

void VoiceGen::on_ctrlm_triggered()
{
    m_ui->menuBar->setVisible(!m_ui->menuBar->isVisible());
}



void VoiceGen::on_ttsthread_finished(const QString error)
{
    if(error.isEmpty())
    {
        static std::filesystem::path save_path;
        static int append;
        if (readsaveflag==ReadSaveFlag::read)
        {
            m_ui->statusBar->showMessage("Reading aloud", 3000);
            return;
        }

        else if (readsaveflag == ReadSaveFlag::saveAs)
        {
            save_path = QFileDialog::getSaveFileName(this,"Save To","/home/").toStdString();
            if (!save_path.empty())std::filesystem::copy("/tmp/voice",save_path);// if the user didn't hit the cancel button in SaveAs dialog
            append = 0;
        }
        else //save
        {
            if (save_path.empty())
            {
                save_path=QFileDialog::getSaveFileName(this,"Save To","/home/").toStdString();
                if (save_path.empty())return ;
            }
            if (std::filesystem::exists(save_path))
            {
                append++;
                std::filesystem::copy("/tmp/voice",save_path.string() + std::to_string(append));
            }
            std::filesystem::copy("/tmp/voice",save_path);
            m_ui->statusBar->showMessage("Saved!", 3000);
        }

    }
    else
    {
        m_ui->statusBar->showMessage("An error occured",2000);
        QMessageBox::critical(this ,"Error",error);
    }

}


void VoiceGen::on_engineCombobox_currentIndexChanged()
{
    // remove vioces from voice combobox
    m_ui->voiceListCombobox->clear();

    //set new voice name for the specific engine
    const std::array<QStringList,4> Voices = {{
        {"English (US)","English (UK)","Italian","Espaniol"}, // SVOX
        {"English (US)","English (UK)","Italian","Espaniol"}, // SVOX (voicegen)
        {" English (US)","Ivy","Joanna","Joey","Justin","Kendra","Kimberly","Matthew","Salli"," English (UK)","Amy","Brian","Emma"," English (AS)","Russell","Nicole"," English (WE)","Geraint"," English (IN)","Aditi","Raveena"},//Streamlabs
    }};
    if (m_ui->engineCombobox->currentIndex() == 2)m_ui->statusBar->showMessage("535 Character limit",3000);
    m_ui->voiceListCombobox->addItems(Voices[m_ui->engineCombobox->currentIndex()]);
}



void VoiceGen::on_actionRead_triggered()
{
    player->stop();
    if (tts->isRunning()) tts->terminate();
    UserTextInput =  m_ui->main_input->toPlainText();
    if (UserTextInput.isEmpty())
    {
        QMessageBox::warning(this ,"No Input","Please write something into the text field!");
    }
    else
    {
        tts->runTTS(m_ui->engineCombobox->currentIndex(),m_ui->voiceListCombobox->currentText(),UserTextInput,true);//in thread
        m_ui->statusBar->showMessage("Loading...");
        readsaveflag=ReadSaveFlag::read;
    }
}



void VoiceGen::on_actionStop_triggered()
{
    player->stop();
    m_ui->statusBar->showMessage("");
    if (tts->isRunning())
    {
        tts->terminate();
        m_ui->statusBar->showMessage("Process cancelled",2000);
    }
}

void VoiceGen::on_actionSave_As_triggered()
{
    if (m_ui->main_input->toPlainText().isEmpty())
    {
        QMessageBox::warning(this, "No Input", "Please write somthing into the text field!");
        return;
    }
    m_ui->statusBar->showMessage("Getting data...",3000);
    readsaveflag=ReadSaveFlag::saveAs;
    tts->runTTS(m_ui->engineCombobox->currentIndex(),m_ui->voiceListCombobox->currentText(),m_ui->main_input->toPlainText(),false);
}


void VoiceGen::on_actionSave_triggered()
{
    if (m_ui->main_input->toPlainText().isEmpty())
    {
        QMessageBox::warning(this, "No Input", "Please write somthing into the text field!");
        return;
    }
    m_ui->statusBar->showMessage("Getting data...",3000);
    readsaveflag=ReadSaveFlag::save;
    tts->runTTS(m_ui->engineCombobox->currentIndex(),m_ui->voiceListCombobox->currentText(),m_ui->main_input->toPlainText(),false);
}


void VoiceGen::on_actionAbout_triggered()
{
    QMessageBox::about(this, "About VoiceGen", "<p>VoiceGen is a simple piece of software for reading your texts.</p> <p>By PersianGolf</p> <a href=\"https://gitlab.com/PersianGolf/voicegen/-/tree/main#buy-me-a-coffee-\">Source code and Coffee☕</a> <p>This project is licensed under the <a href='https://gitlab.com/PersianGolf/voicegen/-/blob/main/LICENCE.md'>GNU GPL v3.0</a>.</p>");
}

void VoiceGen::on_actionQuit_triggered()
{
    delete tts;
    delete player;
    this->close();
}

