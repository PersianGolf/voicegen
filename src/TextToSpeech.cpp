#include "TextToSpeech.h"
#include "StreamElementsCommunication.hpp"
#include <QMediaPlayer>
#include <filesystem>
#include <qobjectdefs.h>
#include <qurl.h>
#include <unordered_map>
#include <string>


TextToSpeech::TextToSpeech(QMediaPlayer* p):
engineIndex(0), //just to be inited
streamflag(false)
{
    player = p;
}


void TextToSpeech::runTTS(const int e, const QString &n,const QString &w, const bool sf)
{
    engineIndex = e;
    VoiceName= n;
    whattosay = w;
    streamflag = sf;
    start();
}


void TextToSpeech::run(void)
{
    whattosay.replace('\n',", ");
    switch(engineIndex)
    {
        case 2:{//Streamlabs
            const std::unordered_map<std::string,const QString> streamlabs_voice_aliases{
            {" English (US)","Matthew"},
            {" English (UK)","Amy"},
            {" English (AS)","Russell"},
            {" English (WE)","Geraint"},
            {" English (IN)","Aditi"},};
            //convert alias to voice name
            if(const auto alias = streamlabs_voice_aliases.find(VoiceName.toStdString());alias != streamlabs_voice_aliases.end())
               VoiceName = alias->second;
            
            if(!StreamElementsCommunication::get_file(VoiceName, whattosay))
            {
                emit taskFinished("Couldn't connect to the server.\nMaybe you are not connected to the Internet or the servers are down.\nOr probably your input is too long for this engine. In this case, use NanoTTS instead.");
            }
            if (streamflag)
            {
                player->setMedia(QUrl::fromLocalFile("/tmp/voice"));
                player->play();
            }
            emit taskFinished();
            break;
        }
        default:{//Linux = 0
            const std::unordered_map<std::string , const QString> voice_name_cmd{
                {"English (US)","en-US"},
                {"English (UK)","en-GB"},
                {"Italian","it-IT"},
                {"Espaniol","es-ES"},
            };
            VoiceName = voice_name_cmd.at(VoiceName.toStdString());
            whattosay.replace('\'',"\'\\\'\'");//   '\''

            const auto nanotts_command = "echo \'"+  whattosay +"\' | nanotts -w -o /tmp/voice.wav --volume 0.45 --voice " + VoiceName;
            const auto result_cmd = std::system(nanotts_command.toStdString().c_str());
            if(result_cmd != 0)
            {
                emit taskFinished("Nanotts backend failed to execute correctly.\nThis happens when your Nanotts executable have issues to run properly or It can't load its voice modules.");
                return;
            }

            if (engineIndex==1)//add enhacement
            {
                (void)!std::system("ffmpeg -nostats -loglevel 0 -y -i /tmp/voice.wav -af \"firequalizer=gain_entry='entry(0,-7);entry(250,-4);entry(1000,2);entry(4000,6);entry(16000,12)'\" /tmp/output.wav");
                std::filesystem::remove("/tmp/voice.wav");
                std::filesystem::rename("/tmp/output.wav","/tmp/voice");
            }
            else std::filesystem::rename("/tmp/voice.wav","/tmp/voice");//workaround
            if (streamflag)
            {
                player->setMedia(QUrl::fromLocalFile("/tmp/voice"));
                player->play();
            }
            emit taskFinished("");
        }
    }
}





