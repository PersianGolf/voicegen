#include "StreamElementsCommunication.hpp"
#include <qurl.h>
#include <qurlquery.h>
#include <filesystem>
#include <tuple> //for std::ignore
#include <format>

bool StreamElementsCommunication::get_file(const QString &voice_name, const QString &whattosay)
{
    QUrl url ("https://api.streamelements.com/kappa/v2/speech");
    const QUrlQuery q {{"voice",voice_name},{"text",QUrl::toPercentEncoding(whattosay)}};
    url.setQuery(q);
    return download_to_tmp(url.toString());
}

bool StreamElementsCommunication::download_to_tmp(const QString& link)
{
    const auto temp_voice_path = std::filesystem::temp_directory_path()/"voice";//  /tmp/voice
    if(std::filesystem::exists(temp_voice_path))std::filesystem::remove(temp_voice_path);
    const auto download_command = std::format("wget \"{}\" -q -O {}",link.toStdString(),temp_voice_path.string());
    std::ignore = std::system(download_command.c_str());
    if(std::filesystem::exists(temp_voice_path) && std::filesystem::file_size(temp_voice_path)!=0)return true;
    return false;
}


