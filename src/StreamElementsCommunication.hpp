#pragma once
// we need to build a URL like this
// https://api.streamelements.com/kappa/v2/speech?voice=Joanna&text=Hello

#include <qurl.h>
#include <QString>
namespace StreamElementsCommunication {
    bool get_file(const QString& voice_name,const QString& whattosay);
    bool download_to_tmp(const QString&);
}